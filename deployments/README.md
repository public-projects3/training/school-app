# Deploy in K8s

```bash
minikube start
# Setup MongoDB DB in K8s
kubectl create ns schoolapp
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install schoolapp-mongodb --namespace schoolapp \
 --set auth.enabled=true \
 --set auth.rootUser=schoolapp \
 --set auth.rootPassword=mongoRootPass \
  bitnami/mongodb

# Add project repo to helm
helm repo add schoolapp https://gitlab.com/api/v4/projects/34240616/packages/helm/stable

# Install the Frontend
helm install frontend -n schoolapp schoolapp/schoolapp-frontend
# Install the API
helm install api -n schoolapp schoolapp/schoolapp-api
# Port forward the frontend
kubectl port-forward service/frontend 8001:80
# Port forward the api
kubectl port-forward service/api 5000:5000
```

