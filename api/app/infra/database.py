# import common.vault as vault
try:
    from icecream import ic
except ImportError:  # Graceful fallback if IceCream isn't installed.
    ic = lambda *a: None if not a else (a[0] if len(a) == 1 else a)  # noqa
import pymongo
import requests
import os
from environs import Env


def printCreds(msg):
    print(f'MongoDB Credentials {msg}: ')
    print(f'Username = {schoolapp_DB_USERNAME}')
    print(f'Password = {schoolapp_DB_PASSWORD}')


def getSecret(filename):
    with open(f"/app/secrets/{filename}") as f:
        secret = f.readlines()
        secret = secret[0].strip('\n')
    return secret


if os.environ['VAULT_STATUS'] == 'kv_static':
    from . import vault
    schoolapp_DB_USERNAME = vault.schoolapp_DB_USERNAME
    schoolapp_DB_PASSWORD = vault.schoolapp_DB_PASSWORD
    printCreds('Using Vault KV with the Vault API. This is a Vault aware app')
elif os.environ['VAULT_STATUS'] == 'kv_static_injector_template_file':
    schoolapp_DB_USERNAME = getSecret("schoolapp-mongodb-username")
    schoolapp_DB_PASSWORD = getSecret("schoolapp-mongodb-password")
    printCreds('Using Vault KV with injector and templates for Vault unaware apps')
elif os.environ['VAULT_STATUS'] == 'kv_static_csi':
    schoolapp_DB_USERNAME = getSecret("schoolapp-mongodb-username")
    schoolapp_DB_PASSWORD = getSecret("schoolapp-mongodb-password")
    printCreds('Using Vault KV and the Vault CSI Provider for Vault unaware apps')
else:
    schoolapp_DB_USERNAME = 'schoolapp'
    schoolapp_DB_PASSWORD = 'mongoRootPass'
    printCreds('Using Hardcoded Values that appear in GitLab')


env = Env()
env.read_env()

ENV = os.environ['ENV']

if ENV == 'PROD':
    SERVER = env('DB_SERVER_PROD')
    PORT = env('DB_PORT_PROD')
    VAULT_URL = env('VAULT_URL_PROD')
else:
    SERVER = env('DB_SERVER_DEV')
    PORT = env('DB_PORT_DEV')
    VAULT_URL = env('VAULT_URL_DEV')

URI = f'mongodb://{schoolapp_DB_USERNAME}:{schoolapp_DB_PASSWORD}@{SERVER}:{PORT}'
myclient = pymongo.MongoClient(URI)

COLLECTION_STUDENTS = os.environ['COLLECTION_STUDENTS']
COLLECTION_COURSES = os.environ['COLLECTION_COURSES']

DB_NAME = os.environ['DB_NAME']
mydb = myclient[DB_NAME]
studentscol = mydb[COLLECTION_STUDENTS]
coursescol = mydb[COLLECTION_COURSES]
