import hvac
import os
import base64
from environs import Env

env = Env()
env.read_env()

ENV = os.environ['ENV']
if ENV == 'PROD':
    VAULT_URL = env('VAULT_URL_PROD')
    with open("/app/secrets/token") as f:
        VAULT_TOKEN = f.readlines()
        VAULT_TOKEN = VAULT_TOKEN[0].strip('\n')
if ENV == 'DEV':
    VAULT_URL = env('VAULT_URL_DEV')
    VAULT_TOKEN = os.environ['VAULT_TOKEN']

print(f'Vault token = {VAULT_TOKEN}')
client = hvac.Client(url=VAULT_URL, token=VAULT_TOKEN)
# print(client.is_authenticated())
# print(client.lookup_token())

# Read the data written under path: internal/data/schoolapp/mongodb
read_response = client.secrets.kv.v2.read_secret_version(
    path='schoolapp/mongodb', mount_point='internal')
schoolapp_DB_USERNAME = read_response['data']['data']['schoolapp_DB_USERNAME']
schoolapp_DB_PASSWORD = read_response['data']['data']['schoolapp_DB_PASSWORD']
