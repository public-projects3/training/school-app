try:
    from icecream import ic
except ImportError:  # Graceful fallback if IceCream isn't installed.
    ic = lambda *a: None if not a else (a[0] if len(a) == 1 else a)
# from fastapi import APIRouter, Depends, HTTPException
from posixpath import join
from typing import Optional, List, Tuple
import datetime
from fastapi import APIRouter, Depends
from pydantic import BaseModel
from ..infra import database
import uuid
from dateutil.relativedelta import relativedelta

router = APIRouter()


class Course(BaseModel):
    title: Optional[str] = None
    description: Optional[str] = None
    author: Optional[str] = None
    picture: Optional[str] = None
    author_picture: Optional[str] = None
    price: Optional[float] = None


@router.get('/courses')
async def get_courses():
    cursor = database.coursescol.find()
    coursesList = list(cursor)
    return {'results': coursesList}


@router.post("/courses")
async def create_course(course: Course):
    courseDoc = dict(course)
    id = uuid.uuid4().hex
    courseDoc.update({'_id': id})
    docExists = database.coursescol.find_one(
        {'title': course.title})
    if not docExists:
        database.coursescol.insert_one(courseDoc)
        return {'results': courseDoc, 'success': True}
    else:
        return {'results': 'Item already exists', 'success': False}


@router.delete("/courses/{id}")
async def delete_course(id: str):
    database.coursescol.delete_one({'_id': id})
    return {"results": "success"}
