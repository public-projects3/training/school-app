try:
    from icecream import ic
except ImportError:  # Graceful fallback if IceCream isn't installed.
    ic = lambda *a: None if not a else (a[0] if len(a) == 1 else a)  # noqa
import uuid
from fastapi import APIRouter, Depends, HTTPException
from typing import Optional, List, Tuple
from marshmallow.fields import DateTime
from pydantic import BaseModel
from ..infra import database

router = APIRouter()


class Student(BaseModel):
    firstname: Optional[str] = None
    lastname: Optional[str] = None
    email: Optional[str] = None
    address: Optional[str] = None
    creditcard: Optional[str] = None


@router.get('/students')
async def get_students():
    cursor = database.studentscol.find()
    studentsList = list(cursor)
    return {'results': studentsList}


@router.post("/students/{course}")
async def create_student(student: Student, course: str):
    studentDoc = dict(student)
    ic(course)
    id = uuid.uuid4().hex
    studentDoc.update({'_id': id, 'enrolledCourse': course})
    docExists = database.studentscol.find_one(
        {'email': student.email, 'enrolledCourse': course})
    if not docExists:
        database.studentscol.insert_one(studentDoc)
        return {'results': studentDoc, 'success': True}
    else:
        return {'results': 'Item already exists', 'success': False}


@router.delete("/students/{id}")
async def delete_student(id: str):
    database.studentscol.delete_one({'_id': id})
    return {"results": "success"}

