# run: uvicorn main:app --reload --port 5000
import uuid
from .routers import courses, students
from .infra import database
from typing import Optional, List
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from environs import Env
import os


app = FastAPI()
app.include_router(students.router)
app.include_router(courses.router)

env = Env()
env.read_env()

APP_VERSION = os.environ['APP_VERSION']

ENV = os.environ['ENV']

if ENV == 'PROD':
    ORIGINS = os.environ['CORS_PROD']
    origins = [ORIGINS]
    domain_url = ORIGINS
if ENV == 'DEV':
    origins = [
        "http://localhost:8080",
        'http://127.0.0.1:8080',
    ]
    domain_url = origins[0]

print('CORS is: ', domain_url)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return "Welcome to the School App API."

@app.get("/version")
def get_app_version():
    return APP_VERSION
