#!/bin/bash
export ENV=DEV
export VAULT_TOKEN=<VAULT_TOKEN>
export DB_NAME=schoolappdb
export COLLECTION_STUDENTS=students
export COLLECTION_COURSES=courses

uvicorn app.main:app --reload --port 5000
