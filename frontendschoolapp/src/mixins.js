import moment from "moment";

export default {
  methods: {
    numberFormatter(number) {
      var formatter = new Intl.NumberFormat("en-CA", {
        style: "currency",
        currency: "CAD",
        minimumFractionDigits: 0
      });
      return formatter.format(number);
    },
    numberFormatterCurrency(number, currency) {
      var locale = "en-CA";
      if (currency == "CAD") locale = "en-US";
      if (currency == "USD") locale = "en-CA";

      var formatter = new Intl.NumberFormat(locale, {
        style: "currency",
        currency: currency,
        minimumFractionDigits: 0
      });
      return formatter.format(number);
    },
    quantityFormatter(number) {
      number = Math.abs(number);
      var formatter = new Intl.NumberFormat("en-CA", {
        minimumFractionDigits: 0
      });
      return formatter.format(number);
    },
    percentageFormatter(number) {
      var formatter = new Intl.NumberFormat("en-CA", {
        minimumFractionDigits: 0,
        maximumFractionDigits: 2
      });
      return formatter.format(number);
    },
    dateFormatter(item, year, month) {
      var options;
      var d;
      if (this.transactions.timeRange == "days") {
        options = { year: "numeric", month: "long", day: "numeric" };
        d = new Date(item);
        return d.toLocaleDateString("en-US", options);
      }
      if (this.transactions.timeRange == "months") {
        options = { year: "numeric", month: "long" };
        d = new Date(year, month);
        return d.toLocaleDateString("en-US", options);
      }
    },
    anotherDateFormatter(item, type) {
      var options;
      var d;
      if (type == "days") {
        options = { year: "numeric", month: "long", day: "numeric" };
        d = new Date(item);
        return d.toLocaleDateString("en-US", options);
      }
      if (type == "months") {
        return moment(item).format("MMMM YYYY");
      }
    },
    formatDate(item) {
      return moment(item).format("YYYY-MM-DD");
    },
    capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    },
    combinedCurrencyCalc(myArray, exchange) {
      // we convert everything to USD and then do a final conversion
      var total = 0;
      var conversion = null;
      // console.log("## mixin array", myArray)
      // console.log("## mixin exchange", exchange)
      for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].currency === "USD") total += myArray[i].amount;
        else {
          conversion = "USD" + myArray[i].currency;
          total += myArray[i].amount / exchange.exchange[conversion];
        }
      }
      if (exchange.currencySelected != "USD") {
        conversion = "USD" + exchange.currencySelected;
        total = total * exchange.exchange[conversion];
      }
      return total;
    },
    convertCurrency(value, exchange) {
      // value you get is always in USD this is used in all trends aggregate calculations and for investment performance
      var conversion = null;
      if (exchange.currencySelected === "USD") return value;
      else {
        conversion = "USD" + exchange.currencySelected;
        return value * exchange.exchange[conversion];
      }
    }
  }
};
