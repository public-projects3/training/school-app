export default {
  getCourses(state, payload) {
    state.courses = payload;
  },
  getStudents(state, payload) {
    state.students = payload;
  }
};
