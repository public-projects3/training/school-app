import axios from "axios";

export default {
  getCourses(context) {
    axios
      .get(`${process.env.VUE_APP_API_ADDRESS}/courses`)
      .then(res => {
        console.log("courses from backend: ", res.data.results);
        context.commit("getCourses", res.data.results);
      })
      .catch(err => console.log(err));
  },
  getStudents(context) {
    axios
      .get(`${process.env.VUE_APP_API_ADDRESS}/students`)
      .then(res => {
        console.log("students from backend: ", res.data.results);
        context.commit("getStudents", res.data.results);
      })
      .catch(err => console.log(err));
  }
};
