export default {
  courses(state) {
    return {
      courses: state.courses
    };
  },
  students(state) {
    return {
      students: state.students
    };
  }
};
