import Vue from "vue";
import "./plugins/axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

new Vue({
  el: "#app",
  router,
  store,
  vuetify,
  template: "<App/>",
  render: h => h(App)
});
