# Overview

This is a demo app used in many of my tutorials. It consists of a front-end written in Vuejs, a backend using python and FastAPI, and mongoDB as the database.

